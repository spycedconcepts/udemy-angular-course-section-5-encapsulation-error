# Udemy Angular Course - Section 5

## Issues

### 1.  Build error on adding encapsulaton to server-element.components.ts

The error occurs when I first add the encapsulation: ViewEncapsulation.None as directed in lecture 71.   The full text of the error, which is the first error reported in Chrome console is:

```
Uncaught Error: Cannot find module 'tslib'
    at webpackEmptyContext (src sync:2)
    at core.js:19
    at core.js:10
    at Object../node_modules/@angular/compiler/src/core.js (core.js:16)
    at __webpack_require__ (bootstrap:78)
    at Module../src/app/server-element/server-element.component.ts (main.js:336)
    at __webpack_require__ (bootstrap:78)
    at Module../src/app/app.module.ts (app.component.ts:8)
    at __webpack_require__ (bootstrap:78)
    at Module../src/main.ts (main.ts:1)
```

There are also a series of warnings reported in the Chrome console:

```
[WDS] Warnings while compiling.
warnings @ client:176
onmessage @ socket.js:40
EventTarget.dispatchEvent @ sockjs.js:170
(anonymous) @ sockjs.js:887
SockJS._transportMessage @ sockjs.js:885
EventEmitter.emit @ sockjs.js:86
WebSocketTransport.ws.onmessage @ sockjs.js:2961
wrapFn @ zone.js:1281
push../node_modules/zone.js/dist/zone.js.ZoneDelegate.invokeTask @ zone.js:423
push../node_modules/zone.js/dist/zone.js.Zone.runTask @ zone.js:195
push../node_modules/zone.js/dist/zone.js.ZoneTask.invokeTask @ zone.js:498
invokeTask @ zone.js:1693
globalZoneAwareCallback @ zone.js:1719
client:185 ./node_modules/@angular/compiler/src/core.js 10:24-31
Critical dependency: require function is used in a way in which dependencies cannot be statically extracted
warnings @ client:185
onmessage @ socket.js:40
EventTarget.dispatchEvent @ sockjs.js:170
(anonymous) @ sockjs.js:887
SockJS._transportMessage @ sockjs.js:885
EventEmitter.emit @ sockjs.js:86
WebSocketTransport.ws.onmessage @ sockjs.js:2961
wrapFn @ zone.js:1281
push../node_modules/zone.js/dist/zone.js.ZoneDelegate.invokeTask @ zone.js:423
push../node_modules/zone.js/dist/zone.js.Zone.runTask @ zone.js:195
push../node_modules/zone.js/dist/zone.js.ZoneTask.invokeTask @ zone.js:498
invokeTask @ zone.js:1693
globalZoneAwareCallback @ zone.js:1719
client:185 ./node_modules/@angular/compiler/src/selector.js 10:24-31
Critical dependency: require function is used in a way in which dependencies cannot be statically extracted
warnings @ client:185
onmessage @ socket.js:40
EventTarget.dispatchEvent @ sockjs.js:170
(anonymous) @ sockjs.js:887
SockJS._transportMessage @ sockjs.js:885
EventEmitter.emit @ sockjs.js:86
WebSocketTransport.ws.onmessage @ sockjs.js:2961
wrapFn @ zone.js:1281
push../node_modules/zone.js/dist/zone.js.ZoneDelegate.invokeTask @ zone.js:423
push../node_modules/zone.js/dist/zone.js.Zone.runTask @ zone.js:195
push../node_modules/zone.js/dist/zone.js.ZoneTask.invokeTask @ zone.js:498
invokeTask @ zone.js:1693
globalZoneAwareCallback @ zone.js:1719
client:185 ./node_modules/@angular/compiler/src/ml_parser/html_tags.js 10:24-31
Critical dependency: require function is used in a way in which dependencies cannot be statically extracted
warnings @ client:185
onmessage @ socket.js:40
EventTarget.dispatchEvent @ sockjs.js:170
(anonymous) @ sockjs.js:887
SockJS._transportMessage @ sockjs.js:885
EventEmitter.emit @ sockjs.js:86
WebSocketTransport.ws.onmessage @ sockjs.js:2961
wrapFn @ zone.js:1281
push../node_modules/zone.js/dist/zone.js.ZoneDelegate.invokeTask @ zone.js:423
push../node_modules/zone.js/dist/zone.js.Zone.runTask @ zone.js:195
push../node_modules/zone.js/dist/zone.js.ZoneTask.invokeTask @ zone.js:498
invokeTask @ zone.js:1693
globalZoneAwareCallback @ zone.js:1719
client:185 ./node_modules/@angular/compiler/src/ml_parser/tags.js 10:24-31
Critical dependency: require function is used in a way in which dependencies cannot be statically extracted
warnings @ client:185
onmessage @ socket.js:40
EventTarget.dispatchEvent @ sockjs.js:170
(anonymous) @ sockjs.js:887
SockJS._transportMessage @ sockjs.js:885
EventEmitter.emit @ sockjs.js:86
WebSocketTransport.ws.onmessage @ sockjs.js:2961
wrapFn @ zone.js:1281
push../node_modules/zone.js/dist/zone.js.ZoneDelegate.invokeTask @ zone.js:423
push../node_modules/zone.js/dist/zone.js.Zone.runTask @ zone.js:195
push../node_modules/zone.js/dist/zone.js.ZoneTask.invokeTask @ zone.js:498
invokeTask @ zone.js:1693
globalZoneAwareCallback @ zone.js:1719
```

This is preceded by an warning report during the build output

```
i ｢wdm｣: Compiling...

Date: 2019-06-30T12:51:17.101Z - Hash: 02ed2717d2f74fc24efc - Time: 262ms
5 unchanged chunks
chunk {main} main.js, main.js.map (main) 22.1 kB [initial] [rendered]

WARNING in ./node_modules/@angular/compiler/src/core.js 10:24-31
Critical dependency: require function is used in a way in which dependencies cannot be statically extracted

WARNING in ./node_modules/@angular/compiler/src/selector.js 10:24-31
Critical dependency: require function is used in a way in which dependencies cannot be statically extracted

WARNING in ./node_modules/@angular/compiler/src/ml_parser/html_tags.js 10:24-31
Critical dependency: require function is used in a way in which dependencies cannot be statically extracted

WARNING in ./node_modules/@angular/compiler/src/ml_parser/tags.js 10:24-31
Critical dependency: require function is used in a way in which dependencies cannot be statically extracted
i ｢wdm｣: Compiled with warnings.
```

##  Angular Readme stuff

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
